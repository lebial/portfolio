import React, { Component } from 'react';
import { categories } from './data.js';
import './App.css';

import Home from './components/content-area/pages/home/home';
import SideBar from './components/sidebar/sidebar';
import ContentArea from './components/content-area/contentArea';
import ProyectIndex from './components/proyectsIndex/proyectIndex';

class App extends Component {
  state = {
    selectedCategory: categories[0],
    selectedProyect: null,
    width: '0',
  }

  changeSelectedCategory = this.changeSelectedCategory.bind(this);
  updateWindowWidth = this.updateWindowWidth.bind(this);
  changeSelectedProyect = this.changeSelectedProyect.bind(this);

  componentDidMount(){
    this.updateWindowWidth();
    window.addEventListener('resize', this.updateWindowWidth);
  }

  changeSelectedCategory(selection) {
    let activeCategorie = categories.find(obj => obj.id === selection);
    this.setState({ selectedCategory: activeCategorie, selectedProyect: null});
  };

  changeSelectedProyect(selection) {
    const activeCategory = this.state.selectedCategory;
    const activeProyect = activeCategory.proyects.find(obj => obj.id === selection);
    this.setState({ ...this.state, selectedProyect: activeProyect });
  }

  updateWindowWidth(){
    this.setState({ width: window.innerWidth })
  }
  render() {
    const image = './hbrs-pic.jpg';
    const selectedCat = this.state.selectedCategory;
    const selectedProyect = this.state.selectedProyect;
    const categoryProyects = selectedCat.proyects;

    return (
      <div className="App">
        <SideBar data={ categories }
                 handleSelect={ this.changeSelectedCategory }
                 selected={ selectedCat }
                 image={ image }
                 width={this.state.width}/>

        {selectedCat.id === 'Home' ?
          <Home width={this.state.width} /> : selectedProyect !== null ?
            <ContentArea data={selectedProyect} width={this.state.width}/> :
            <ProyectIndex proyects={ categoryProyects } handleProyectSelection={this.changeSelectedProyect}/>
        }
      </div>
    );
  }
}

export default App;

