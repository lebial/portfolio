import React from 'react';
import PropTypes from 'prop-types';

import Row from './row/row';
import ContentImage from './contentImage/contentImage';
import ContentDescription from './contentDescription/contentDescription';

import './contentArea.css';
import './row/row.css';

const ContentArea = ({ data, width }) => {
  let colPosition = 0;
  const incrementPosition = () => { colPosition += 1; };
  const containerWidth = width > 800 ? '50%' : '100%';
  const widthStyle = { width: containerWidth };
  const sortedRows = data.details.map(detail => (

    <Row key={detail.title}>
      <div className="content__column" style={widthStyle}>
        { colPosition % 2 === 0 ? <ContentImage images={detail.images} /> : <ContentDescription title={detail.title} description={detail.description} button={detail.button} /> }
      </div>
      <div className="content__column" style={widthStyle}>
        { colPosition % 2 === 1 ? <ContentImage images={detail.images} /> : <ContentDescription title={detail.title} description={detail.description} button={detail.button} /> }
      </div>
      {incrementPosition()}
    </Row>
  ));

  const rows = data.details.map(detail => (
    <Row key={detail.title}>
      <div className="content__column" style={widthStyle}>
        <ContentDescription title={detail.title} description={detail.description} button={detail.button}>
          <ContentImage images={detail.images} />
        </ContentDescription>
      </div>
    </Row>
  ));

  return (
    <div className="content-area__container" >
      <div className="proyect-index__title">{data.title}</div>
      { width < 800 ? rows : sortedRows }
    </div>
  );
};

ContentArea.propTypes = {
  selectedProyect: PropTypes.string.isRequired,
};

export default ContentArea;
