import React from 'react';
import './contentDescription.css';

const ContentDescription = ({
  description, title, button, children,
}) => {
  const textArray = description.split(/(?:\.(?=[A-Z]))/g);
  const InfoButton = ({ url, text }) => (
    <div className="description__button">
      <a href={url} target="_blank">{text}</a>
    </div>
  );

  return (
    <div className="description__container">
      <div className="description__title">
        <p>{ title }</p>
      </div>
      <div>
        {children}
      </div>
      <div className="description__body">
        {textArray.map(text => (
          <span>{`${text}.`}</span>
				))}
      </div>
      { button && <InfoButton url={button.url} text={button.text} /> }
    </div>
  );
};

export default ContentDescription;
