import React, { Component }from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';

import ImageSlide from './imageSlide/imageSlide';
import './contentImage.css';

class ContentImage extends Component {
  state = {
    imageIndex: 0,
  }
  changeCurrentImage = this.changeCurrentImage.bind(this);

  changeCurrentImage(direction){
  let currentIndex = this.state.imageIndex;
  const sizing = this.props.images.length
    if (direction === 'right' && currentIndex === sizing - 1){
      this.setState({ imageIndex: 0 });
      return;
    } else if (direction === 'left' && currentIndex === 0)  {
        this.setState({ imageIndex: sizing - 1 });
       return;
      }
     currentIndex = direction === 'right' ? currentIndex + 1 : currentIndex - 1;
     this.setState({ imageIndex: currentIndex });
  };

  render() {
  const images = this.props.images;
  const currentImage = this.state.imageIndex;
  const showArrows = images.length > 1;
    return(
      <div className="image__container" >
        <TransitionGroup className="">
          <CSSTransition key={ currentImage } timeout={500} classNames="move">
            <ImageSlide key={ currentImage }
                        src={ images[currentImage].src }
                        alt={ images[currentImage].alt }
                        handleChange={ this.changeCurrentImage }
                        showArrows={showArrows}/>
          </CSSTransition>
        </TransitionGroup>
      </div>
    );
  };
}

export default ContentImage;
