import React from 'react';
import { CSSTransition } from 'react-transition-group';

const Arrows = ({ handleChange }) => (
  <div>
    <span
      className="image__arrow--left"
      onClick={() => handleChange('left')}
    >
      <i className="fas fa-caret-left" />
    </span>
    <span
      className="image__arrow--right"
      onClick={() => handleChange('right')}
    >
      <i className="fas fa-caret-right" />
    </span>
  </div>
);


const ImageSlide = ({
  src, alt, handleChange, showArrows,
}) => (
  <div>
    <img className="content__image" src={src} alt={alt} />
    {showArrows && <Arrows handleChange={handleChange} />}
    <div className="content__image__shadow" />
  </div>
);

export default ImageSlide;

