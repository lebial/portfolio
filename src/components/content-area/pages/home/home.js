import React from 'react';
import './home.css';

const Home = ({ width }) => {
  const images = ['./enie-swirl.png', 'hbrs-pic.jpg'];

  return (
    <div className="content-area__container">
      <div className="home__container">
        <div className="home__content">
          <div className="home__title">
            <p>Thank you for visiting</p>
          </div>
          <div className="home__logo">
            <img src={width > 800 ? images[0] : images[1]} alt="enie logo" />
          </div>
          <div className="home__subtitle">
            <h2>Versatile designer with a passion for creative problem solving.</h2>
          </div>
          <div className="home__description">
            <p>
            I have worked in the design industry for 13 years. During this time, I’ve been able to improve my skills by taking on new challenges that pushed my limits and ultimately made me a better, more empathetic designer.
            </p>
            <p>
            I’m a self-motivated learner that enjoys good design in the world and I truly hope I get the opportunity to contribute to your organization.
            </p>
          </div>
          <div className="home__contact">
            <div><a href="mailto:hector@eniedesign.com">hector@eniedesign.com </a><span>''</span></div>
            <div><a href="tel:+17735771333">+1(773) 577-1333 </a><span>''</span></div>
            <div><a id="linked" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/hector-b-rangel/">in</a></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Home;
