import React from 'react';
import './row.scss';

const Row = ({ children }) => (
  <div className="content__row">
    { children }
  </div>
);

export default Row;
