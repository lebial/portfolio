import React from 'react';
import './proyectContainer.css';

const ProyectContainer = ({
  handleProyectSelect, src, alt, id, description,
}) => {
  const textArray = description.split(/(?:\.(?=[A-Z]))/g);
  const styles = { backgroundImage: `url(${src})` };
  return (
    <div className="proyect__container">
      <div className="proyect__container__title"><p>{ alt }</p></div>
      <div className="proyect__container__image" style={styles} />
      <div className="proyect__container__overlay">
        { textArray.map(text => (<p>{`${text}.` }</p>))}
        <button onClick={() => handleProyectSelect(id)}>More info</button>
      </div>
    </div>
  );
};

export default ProyectContainer;
