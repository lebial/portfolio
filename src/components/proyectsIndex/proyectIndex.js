import React from 'react';
import ProyectContainer from './proyectContainer/proyectContainer';
import Row from '../content-area/row/row';
import './proyectIndex.css';

const ProyectIndex = ({ proyects, handleProyectSelection }) => {
  const contentStyles = {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  };

  const ProyectsList = proyects.map(proyect => (
    <Row key={proyect.id}>
      <div className="content__column" style={contentStyles}>
        <ProyectContainer
          key={proyect.id}
          handleProyectSelect={handleProyectSelection}
          src={proyect.image}
          alt={proyect.title}
          id={proyect.id}
          description={proyect.description}
        />
      </div>
    </Row>
  ));

  return (
    <div className="content-area__container">
      <Row>
        <div className="proyect-index__title">Projects</div>
      </Row>
      { ProyectsList }
    </div>
  );
};

export default ProyectIndex;
