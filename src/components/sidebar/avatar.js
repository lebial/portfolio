import React from 'react';
import PropTypes from 'prop-types';

import './avatar.css';

const Avatar = ({ img, styling }) => (
  <div className="avatar">
    <img src={img} className={styling} alt="Hector Rangel" />
  </div>
);

Avatar.propTypes = {
  img: PropTypes.string.isRequired,
  styling: PropTypes.string.isRequired,
};

export default Avatar;
