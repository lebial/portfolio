import React from 'react';
import PropTypes from 'prop-types';
import './categorie.css';

const Categorie = ({
  id, content, href, selected, handleSelect,
}) => {
  const styling = `sidebar__categorie ${selected ? 'sidebar__categorie--selected' : ''}`;
  return (
    <li className={styling}>
      <a
        className="sidebar__categorie--text"
        href={href}
        onClick={() => handleSelect(id)}
      >
        { content }
      </a>
    </li>
  );
};

Categorie.propTypes = {
  id: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  href: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  handleSelect: PropTypes.func.isRequired,
};
export default Categorie;
