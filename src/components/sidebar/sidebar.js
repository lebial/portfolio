import React from 'react';
import PropTypes from 'prop-types';
import Avatar from './avatar';
import './sidebar.css';
import { slide as Menu } from 'react-burger-menu';
import Categorie from './categorie';


const SideBar = ({
  image, handleSelect, data, selected, width,
}) => {
  const categorieElements = data.map((cat) => {
    const selection = selected.id === cat.id;
    return (
      <Categorie
        key={cat.id}
        id={cat.id}
        content={cat.content}
        selected={selection}
        href="#"
        handleSelect={handleSelect}
      />
    );
  });

  const sideBar = () => (
  <div className="sidebar__container">
    <div className="sidebar__body">
      <Avatar
        img={image}
        styling="avatar__image"
      />
      <div className="sidebar__userName" >
        <h1>Hector B Rangel</h1>
      </div>
      <ul className="sidebar__categories">
        { categorieElements }
      </ul>
    </div>
    </div>
  );

  const menu = () => (
  <div>
    <Menu width={'80%'}>
      { categorieElements }
    </Menu>
  </div>
  );


  return (
     width > 800 ? sideBar() : menu()
  );
};

// SideBar.propTypes = {
//   image: PropTypes.string.isRequired,
//   handleSelect: PropTypes.func.isRequired,
//   selected: PropTypes.string.isRequired,
//   data: PropTypes.arrayOf(PropTypes.object).isRequired,
// };

export default SideBar;
