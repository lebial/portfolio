// sling-jet
const slingProyectDescription = 'For this project I was tasked with designing a foam airplane that would loop when launched. I had to learn the basics of aerodynamics to be able to attempt to design something that could fly. Each failed attempt brought me closer to finding the correct design solution for this challenge';

// design and prototyping verviages
const slingDesignPropDescription = 'For this project I was tasked with designing a foam airplane that would loop when launched. I had to learn the basics of aerodynamics to be able to attempt to design something that could fly. Each failed attempt brought me closer to finding the correct design solution.Since we weren’t producing the prototypes in house, I created technical drawings with slight variations in the wings so the factory in China could produce the prototypes for us. At this point I also started creating some color schemes for the plane';

// testing polishin finishing verbiages
const slingTesingFinish = 'After receiving the prototype planes, it was time for testing. We experimented with the weight distribution of the plane using pennies and tape to find the best placing of the small penny stack. After some trial & error we accomplished the goal of making the plane loop after launch.Once testing was done I made the revised technical drawings to send to China for production. There is no official name for this product yet, but I call it SlingJet';

// Hydro Flame Water Gun

const waterGunProyectDescription = 'This was my favorite physical product project to work on because I was involved in the whole process of designing a toy, and the brand name, colors, packaging, etc. I was also able to iterate on the water gun’s design several times which allowed me to fine tune the product for our target users (children). I also got the opportunity to observe the users playing with the gun at a waterpark which gave me great insights to improve the design and thus, improving the general UX of the product.';

const waterGunDesignChallenge = 'For this project I was handed a standard water launcher that was previously produced in our factory. The design challenge was to find a way to include LED’s in the nozzle head and to make it appealing to young children. The constraints of this project enabled me to narrow down into viable solutions. I started brainstorming and did some simple sketches.Once a good direction for the design was found, I started making the technical drawings for the prototypes to be made overseas. After some back and forward a prototype design was approved and was sent to production';

const waterGunUX = 'After receiving the prototypes, it was time for usability testing. I took a couple water guns to our local waterpark and tested them with children. After observing closely, I noticed that most of the kids struggled to fill the water tube in it’s entirety because it was too long, and this was hindering the overall user experience.It was evident to me that we needed to shorten the water tube to accommodate shorter arms, and we also needed to adjust the placement of the light-up button to be easily accessible by the thumb. I updated the technical drawings to reflect these changes and sent them back to the factory.At this point I also started experimenting with different color schemes for the water guns';

const waterGunBranding = 'At this stage of the project I was given the name & tagline for the product and was tasked with creating the whole image for the brand including the logo and the label. I started the process by acquiring the “hero” pics of the children -one boy and one girl, as requested by the owners.Once I located the appropriate pictures I photoshopped our own guns into the pictures. The final step was to add the lit-up stream of water to each pic.I then created different logo options for the stakeholders to choose from. Once the logo was finalized I made the label for the gun. At this moment the gun is completed and is set to hit the market in early 2019';

// Beach Ball
const beachBallProyectDescription = 'This project holds a special place in my heart because it was the first physical product that I ever designed. In the surface it appeared a very simple task but designing a working solution was quite challenging and rewarding. I got to design the product, the packaging dielines for production, the artwork for all the packaging & labels';

const beachBallChallenge = 'The task was to find a solution to keep the LEDs at the center of the ball while preserving the balance of the sphere. The ball needed to feel like a normal ball when tossed around, other protypes had the weight on one end of the ball and it caused it to wobble and made its trajectories unpredictable since it’s mass wasn’t balanced.After a couple prototypes and a lot of brainstorming we reached a viable solution. The battery casing with the on-off switch was placed on one end, a counter weight was put on the opposite end; between the two we placed an elastic string across the whole diameter of the ball. The LED lights were placed in the center of the string so when the ball is fully inflated the LEDs stayed at the center of the sphere';

const beachBallFinal = 'To be able to design the technical drawings for the factory I replicated the design with a different ball and resized it to meet our needs. I also created the decoration patterns for the ball. Once everything was ready we sent everything to the factory in china to get ready for production.The next task was to design the individual packaging of each ball and the pdq display for the balls. The only constraint was that the ball needed to fit in a specific resealable bag and the display box needed to fit 24 folded balls in two columns.Having these constraints in place enabled me to find a quick solution. I just needed to figure out the best way to fold the ball to fit in the amount of space we had available. Once that was done I presented the idea to the stakeholders and it was approved.The final step was to design the technical drawings for PDQ Display so the factory could produce the box. The final step was to create the artwork for both the pdq and the individual bag’s labels';

// Ux Design
// Torres Omar
const torresProyectDescription = 'This has been one of the longest projects I have embarked on. It all started with a Flash based website I developed back in 2008. In the next 7 years I created different versions of the website, the last version I built was finished in late 2014. I used bootstrap, html5 and css to get that done.The website itself was quite good but we had several usability problems. I needed to free up my time so I could focus on improving the overall UX which is why we migrated to an e-commerce platform that had customizable templates. This allowed me to redesign the website, putting good UX at the center of our efforts, without having to worry about building / maintaining the whole website';

const torresPreviousWeb = 'The first project I did for this customer was redesigning their logo. The owner didn’t like the script font of their current logo and he wanted something more conservative but wanted to retain the crown portion of the old logo.  I ended up going with a serif font to make the text easier to read and stylized the crown to make it a bit more streamlined while retaining the overall appearance of the original one.I also got tasked with designing the jewelry’s website several times. Going from a simple flash-based website to an ecommerce website with html and css. In the last redesign I also created a mobile version of the site. Back then you basically needed to create two separate versions of the website to have a desktop and mobile site, so it was twice the work to maintain such sites';

const torresResponsive = 'Maintaining two separate versions of the website was no longer viable and I needed to look for a better solution for a responsive website. After doing some research I reached the conclusion that the best way to achieve this was to use bootstrap, html5 & CSS.After the redesign was done, my main priority was to grow the online store’s inventory. In a little under a year I was able to significantly grow the online inventory of the store. I oversaw the whole process, from shooting the item pictures and digitally retouching them, to creating each individual product listing and publishing it online.Regardless of the new website and bigger product selection, we weren’t really selling any items. After reviewing analytics data and gathered feedback from our users, we discovered that our categories were too confusing and basically the users weren’t finding what they wanted right away, so they left';

const torresFocus = 'After realizing that the core of our problems was bad user experience. Our priority from that point forward was to give our customers the best UX we could. This was going to consume more of my time so we migrated our e-commerce store to bigcommerce because it has integrated analytics and it offers customizable responsive templates. Thanks to this transition I had the time available to work with the owners to improve the overall online shopping experience.We did card sorting exercises to find the best way to group items in categories that made sense to the users. I shadowed the sales team to learn how they offered the jewelry to the customers in the store so I could attempt to replicate the same process online.After the migration and redesign was done we gradually started getting orders and the conversion rate has been steadily improving. Please click below to visit the current website';

// Cupteria
const cupteriaProyectDescription = 'In my view, the best way to learn is by doing, which is why I designed a Cupcake selling app as a personal project because I was interested in learning prototyping with inVision. Most of my design decisions were based on assumptions that cannot be validated since this product hasn’t been tested but I think it served it’s academic purpose';

const cupteriaDescription = 'One of the things that attracts me to work in digital products is iteration. Being able to ideate, design, test, and learn on a loop which ideally should lead to great products that people solve real problems and are joy to use.I got the inspiration for this project because a family member in Mexico started a small business selling cupcakes and we talked about the idea of selling cupcakes on an app and what the ideal experience for that might be.Please click below to see the prototype';

// Graphic Design
// Editorial Design

const editorialProyectDescription = 'Over the last 14 years I have designed more than 30 issues of a local Hispanic magazine called Chicago Grafico. In the beginning I was tasked with redesigning the look and feel of the magazine.With each issue I try polish the design anywhere I can. At it’s highest point we were publishing 6 issues per year (around 2008). Currently we publish 1 issue per year and is well-received by its readers';

const chicago2018 = 'This is the latest issue of this magazine. Currently, this magazine is published yearly on the last week of September and is designed by me.Click on the button below to checkout this issue';
const chicago2017 = 'This was the 2017 issue, this cover is special to me because the earthquake that devastated Mexico City had just occurred. I tried to honor all the rescuers by dedicating this magazine cover to them.Click on the button below to checkout this issue';
const chicago2016 = 'This was the 2016 issue, this cover was dedicated to Mexican folklore dancers that appeared at the Mexican independence events organized by the Mexican Consulate in Chicago.Click on the button below to checkout this issue';
const chicago2015 = 'The 2015 issue, this cover was also dedicated to Mexican folklore dancers that appeared at the Mexican independence events organized by the Mexican Consulate in Chicago.Click on the button below to checkout this issue';

// Catalog Design
const catalogDesignProyectDescription = 'I had the opportunity to design 4 Catalogs for an established chain of western clothing stores that rely heavily on catalogs to sell their products. Catalog design in my view is very similar to designing E-commerce websites because you need to organize the products in a way that makes sense for the person using it.Designing these catalogs was quite challenging because there were a lot of items to organize. It was through good teamwork that we overcame these challenges and produced catalogs that were generating more revenue for the stores than expected';

const bHip = 'This project started in 2006 and it took almost 8 months to finish. I shot and digitally retouched all the item images used in the catalog.It was very important for the store owner that the colors of the items matched with how they were printed in the catalog, so I had to work with the printers to ensure color accuracy';

const sotoBoots = 'After the success of the first catalog, the store owner wanted to make a new catalog for their western clothing line. Thanks to everything I learned in the first catalog, the process for the second one was much easier.This was a 92-page catalog so it was quite challenging to keep track of everything but I was able to deliver a very good finished product thanks to the people that collaborated with me in the project';

const sotoShoes = 'The owners decided to rebrand from B.hip to Soto Shoes and I created a new catalog with all the new products. This was my favorite catalog because this time around I felt like a fish in the water so everything came together smoothly';

// Logo Design
const logoDesignProyectDescription = 'Even though I’ve had the opportunity to complete several non-print projects like websites, animations or even audio & video editing; most of my work as a freelancer has been for printed media. I was lucky enough to create clothing catalogs and magazines, lots of menus, logos and brochures, while also honing my interpersonal and customer service skills';

const logoProc = 'Most of memorable logos have been done when I\'m explicitly hired to design them because all my time and efforts can be focused on just that specific task. These are some of my favorite ones.';
const moreLogos = 'Cocina Nuevo Leon and Taqueria Anafrez were made for Mexican restaurants. Speed Lab was an Auto Shop that specialized in car tuning. Cake Bros is a bakery that specializes in fondant cakes. Danae Studio is a beauty salon and Horticultural Consulting is pretty much self-explanatory.';

export const categories = [
  {
    id: 'Home',
    content: 'Home',
    proyects: [],
  },
  {
    id: 'productDesign',
    content: 'Product Design',
    proyects: [
      {
        id: 'slingjet',
        title: 'Slingshot Foam Plane',
        image: './productDesign/slingJet/01-A.jpg',
        description: slingProyectDescription,
        details: [
          {
            title: 'Research, Design & Prototyping',
            description: slingDesignPropDescription,
            images: [
              { src: './productDesign/slingJet/01-A.jpg' },
              { src: './productDesign/slingJet/01-B.jpg' },
              { src: './productDesign/slingJet/01-C.jpg' },
            ],
          },
          {
            title: 'Testing, Polishing & Finishing',
            description: slingTesingFinish,
            images: [
              { src: './productDesign/slingJet/02-A.jpg' },
              { src: './productDesign/slingJet/02-B.jpg' },
              { src: './productDesign/slingJet/02-C.jpg' },
            ],
          },
        ],
      },
      {
        id: 'hydroFlame',
        title: 'HydroFlame Water Gun',
        image: './productDesign/hydroBeam/00.jpg',
        description: waterGunProyectDescription,
        details: [
          {
            title: 'Design Challenge',
            description: waterGunDesignChallenge,
            images: [
						  { src: './productDesign/hydroBeam/01-A.jpg' },
						  { src: './productDesign/hydroBeam/01-B.jpg' },
						  { src: './productDesign/hydroBeam/01-C.jpg' },
            ],
          },
          {
            title: 'UX Design',
            description: waterGunUX,
            images: [
						  { src: './productDesign/hydroBeam/02-A.jpg' },
						  { src: './productDesign/hydroBeam/02-B.jpg' },
						  { src: './productDesign/hydroBeam/02-C.jpg' },
            ],
          },
          {
            title: 'Branding, Digital Imaging & Labels',
            description: waterGunBranding,
            images: [
						  { src: './productDesign/hydroBeam/03-A.jpg' },
						  { src: './productDesign/hydroBeam/03-B.jpg' },
						  { src: './productDesign/hydroBeam/03-C.jpg' },
						  { src: './productDesign/hydroBeam/03-D.jpg' },
            ],
          },
        ],
      },
      {
        id: 'beachBall',
        title: 'Light Up Water Ball',
        image: './productDesign/beachBall/00.jpg',
        description: beachBallProyectDescription,
        details: [
          {
            title: 'Challenge, Ideation & Solution',
            description: beachBallChallenge,
            images: [
						  { src: './productDesign/beachBall/01-A.jpg' },
						  { src: './productDesign/beachBall/01-B.jpg' },
						  { src: './productDesign/beachBall/01-C.jpg' },
            ],
          },
          {
            title: 'Polishing, Packaging & Finalization',
            description: beachBallFinal,
            images: [
						  { src: './productDesign/beachBall/02-A.jpg' },
						  { src: './productDesign/beachBall/02-B.jpg' },
						  { src: './productDesign/beachBall/02-C.jpg' },
						  { src: './productDesign/beachBall/02-D.jpg' },
            ],
          },
        ],
      },
    ],
  },
  {
    id: 'ux',
    content: 'UX Design',
    proyects: [
      {
			  id: 'torres',
        title: 'Torres Omar Jewelers',
        image: './uxDesign/torres/00.jpg',
        description: torresProyectDescription,
        details: [
          {
            title: 'Logo Redesign & Previous Website Iterations',
            description: torresPreviousWeb,
            images: [
						  { src: './uxDesign/torres/01.jpg' },
            ],
          },
          {
            title: 'Responsive Web Design',
            description: torresResponsive,
            images: [
						  { src: './uxDesign/torres/02.jpg' },
            ],
          },
          {
            title: 'Focus in User Experience',
            description: torresFocus,
            images: [
						  { src: './uxDesign/torres/03.jpg' },
            ],
            button: {
              url: 'http://www.torresomarjewelry.com/',
              text: 'Visit Page',
            },
          },
        ],
      },
      {
			  id: 'cupteria',
        title: 'Cupteria',
        image: './uxDesign/cupteria/00.jpg',
        description: cupteriaProyectDescription,
        details: [
          {
            title: 'Cupteria App',
            description: cupteriaDescription,
            images: [
						  { src: './uxDesign/cupteria/01.gif' },
						  { src: './uxDesign/cupteria/02.jpg' },
            ],
            button: {
              url: 'http://www.eniedesign.com/cupteria/index.html',
              text: 'Go to Invision',
            },
          },
        ],
      },
    ],
  },
  {
    id: 'graphicDesign',
    content: 'Graphic Design',
    proyects: [
      {
			  id: 'editorial',
        title: 'Editorial Design',
        image: './graphicDesign/editorial/00.jpg',
        description: editorialProyectDescription,
        details: [
          {
            title: 'Chicago Grafico 2018',
            description: chicago2018,
            images: [
						  { src: './graphicDesign/editorial/01.jpg' },
            ],
            button: {
				      url: 'http://online.pubhtml5.com/luxx/vopw/',
              text: 'View Issue',
            },
          },
          {
            title: 'Chicago Grafico 2017',
            description: chicago2017,
            images: [
						  { src: './graphicDesign/editorial/02.jpg' },
            ],
            button: {
				      url: 'http://online.pubhtml5.com/luxx/rryf/',
              text: 'View Issue',
            },
          },
          {
            title: 'Chicago Grafico 2016',
            description: chicago2016,
            images: [
						  { src: './graphicDesign/editorial/03.jpg' },
            ],
            button: {
				      url: 'http://online.pubhtml5.com/luxx/zsnx/',
              text: 'View Issue',
            },
          },
          {
            title: 'Chicago Grafico 2015',
            description: chicago2015,
            images: [
						  { src: './graphicDesign/editorial/04.jpg' },
            ],
            button: {
				      url: 'http://online.pubhtml5.com/luxx/dodg/',
              text: 'View Issue',
            },
          },
        ],
      },
      {
			  id: 'catalog',
        title: 'Catalog Design',
        image: './graphicDesign/catalog/00.jpg',
        description: catalogDesignProyectDescription,
        details: [
          {
            title: 'B.Hip',
            description: bHip,
            images: [
						  { src: './graphicDesign/catalog/01.jpg' },
            ],
            button: {
              url: 'http://online.pubhtml5.com/luxx/nqbh/',
              text: 'View Catalog',
            },
          },
          {
            title: 'Soto Boots',
            description: sotoBoots,
            images: [
						  { src: './graphicDesign/catalog/02.jpg' },
            ],
            button: {
              url: 'http://online.pubhtml5.com/luxx/kkmh/',
              text: 'View Catalog',
            },
          },
          {
            title: 'Soto Shoes',
            description: sotoShoes,
            images: [
						  { src: './graphicDesign/catalog/03.jpg' },
            ],
            button: {
              url: 'http://online.pubhtml5.com/luxx/ceix/',
              text: 'View Catalog',
            },
          },
        ],
      },
      {
			  id: 'logo',
        title: 'Logo Design',
        image: './graphicDesign/logo/00.jpg',
        description: logoDesignProyectDescription,
        details: [
          {
            title: 'Logo Design',
            description: logoProc,
            images: [
						  { src: './graphicDesign/logo/01.jpg' },
            ],
          },
          {
            title: 'Details',
            description: moreLogos,
            images: [
						  { src: './graphicDesign/logo/02-A.jpg' },
						  { src: './graphicDesign/logo/02-B.jpg' },
						  { src: './graphicDesign/logo/02-C.jpg' },
						  { src: './graphicDesign/logo/02-D.jpg' },
						  { src: './graphicDesign/logo/02-E.jpg' },
						  { src: './graphicDesign/logo/02-F.jpg' },
            ],
          },
        ],
      },
    ],
  },
];
